from tools import fibonacci

fibs = fibonacci(10000)
somme = 0
x = next(fibs)
while x < 4000000:
    if x % 2 == 0:
        somme += x
    x = next(fibs)

print(somme)
