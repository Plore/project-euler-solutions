from tools import divisors

abundants = []
for n in range(1, 28124):
    if sum(divisors(n)) > n:
        abundants.append(n)

nums = [1]*28123
for a in range(len(abundants)):
    for b in range(a, len(abundants)):
        if abundants[a] + abundants[b] < 28124:
            nums[abundants[a] + abundants[b] - 1] = 0

print(sum([(i + 1) * nums[i] for i in range(len(nums))]))
