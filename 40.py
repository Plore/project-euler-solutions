s = ""
# 200000 concatenated numbers are clearly enough, since the majority of values > 100000 contribute six digits
for n in range(1, 200000):     
    s += str(n)

print(int(s[0]) * int(s[9]) * int(s[99]) * int(s[999])* int(s[9999]) * int(s[99999]) * int(s[999999]))
