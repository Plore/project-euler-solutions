from tools import is_prime

def truncatable_left(x):
    n = len(str(x))
    for m in range(n):
        right_trunc = x // 10**m
        if not is_prime(right_trunc):
            return False
    return True

def truncatable_right(x):
    n = len(str(x))
    for m in range(n):
        left_trunc = x % 10**(n - m)
        if not is_prime(left_trunc):
            return False
    return True

left = [2, 3, 5, 7]
truncatable_primes_left = [left]
for n in range(5):
    truncatable_primes_left.append([])
    for t in truncatable_primes_left[n]:
        for x in range(1, 10):
            num = int(''.join(str(t) + str(x)))
            if truncatable_left(num):
                truncatable_primes_left[n + 1].append(num)

truncatable_primes = []
tp = [item for sublist in truncatable_primes_left[1:] for item in sublist] # incomprehensible list comprehension to flatten array
for t in tp:
    if truncatable_right(t):
        truncatable_primes.append(t)

#print(truncatable_primes)
print(sum(truncatable_primes))
