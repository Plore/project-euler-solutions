from tools import fibonacci

count = 0
for f in fibonacci(10000):
    count += 1
    if len(str(f)) == 1000:
        break

print(count)
