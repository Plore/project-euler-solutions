def ways(amount, coins):
    if amount == 0:
        return 1
    if coins == [1]:
        return 1
    total = 0
    for i in range(len(coins)):
        rest = amount - coins[i]
        total += ways(rest, [c for c in coins[i:] if c <= rest])
    return total

print(ways(200, [200, 100, 50, 20, 10, 5, 2, 1]))
