from math import sqrt
from fractions import Fraction

def frac(chainfrac_coeffs):
    n = 0
    d = 1
    for k in chainfrac_coeffs[::-1]:
        n += k * d
        n, d = Fraction(n, d).denominator, Fraction(n, d).numerator
    n, d = Fraction(n, d).denominator, Fraction(n, d).numerator
    return n, d

max_x = max_y = max_w = 1

for w in [p for p in range(2, 1001) if not sqrt(p) == int(sqrt(p))]:
    coeffs = []
    c = 1
    n = 0
    d = 1
    x = y = 1
    curr = sqrt(w)
    while not 1 + w * y**2 == x**2:
        coeffs.append(int(curr))
        n -= int(curr) * d
        c = d
        d = w - n**2
        n = -n
        c, d = Fraction(c, d).numerator, Fraction(c, d).denominator
        curr = c * (sqrt(w) + n) / d
        x, y = frac(coeffs)
    if x > max_x:
        max_x = x
        max_y = y
        max_w = w

print("{} * {}^2 + 1 = {}^2".format(max_w, max_y, max_x))
