def cubid(n):
    digits = [c for c in str(n**3)]
    digits.sort()
    return ''.join(digits)

cubes = {}
for i in range(10000):
    cid = cubid(i)
    if not cid in cubes.keys():
        cubes[cid] = [i]
    else:
        cubes[cid].append(i)

mincube = 10000
for cid in cubes.keys():
    if len(cubes[cid]) == 5:
        mincube = min(mincube, min(cubes[cid]))

print(mincube**3)
