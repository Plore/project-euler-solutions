from itertools import permutations

def is_valid(perm):
    s = perm[0] + perm[5] + perm[6]
    for i in range(1, 4):
        if perm[i] + perm[i + 5] + perm[i + 6] != s:
            return False
    if perm[4] + perm[9] + perm[5] != s:
        return False
    return True


maxstring = 0
for p in list(permutations(range(1, 11))):
    if is_valid(p):
        triplets = [[x for x in [p[i], p[i + 5], p[i + 6]]] for i in range(4)]
        triplets.append([p[4], p[9], p[5]])
        while triplets[0][0] != min([t[0] for t in triplets]): # rotate to start with lowest external node
            triplets.append(triplets[0])
            triplets = triplets[1:]
        string = ''.join([''.join([str(x) for x in t]) for t in triplets])
        if len(string) == 16:
            maxstring = max(maxstring, int(string))

print(maxstring)
