from fractions import Fraction

n = 0
d = 1

for k in range(99, 0, -1):
    if k % 3 == 0 or k % 3 == 1:
        c = 1
    else:
        c = int(2 * (k + 1) / 3)
    n += c * d
    n, d = Fraction(n, d).denominator, Fraction(n, d).numerator

n += 2 * d
n, d = Fraction(n, d).numerator, Fraction(n, d).denominator

print(sum([int(c) for c in str(n)]))
