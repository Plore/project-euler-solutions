# while it is sufficient to apply subtraction rule simplifications, this solution 
# provides functions for simplification of general valid roman numerals

literals = {'M':1000, 'D':500, 'C':100, 'L':50, 'X':10, 'V':5, 'I':1}
chars = ['M', 'D', 'C', 'L', 'X', 'V', 'I']

def simple_roman(x):
    res = []
    for c in chars:
        while x >= literals[c]:
            res.append(c)
            x -= literals[c]
    return "".join(res)

def apply_subtraction_rule(numeral):
    res = numeral
    res = res.replace("IIII", "IV")
    res = res.replace("VIV", "IX")
    res = res.replace("XXXX", "XL")
    res = res.replace("LXL", "XC")
    res = res.replace("CCCC", "CD")
    res = res.replace("DCD", "CM")
    return res

def decode_roman(numeral):
    res = 0
    min_char = 1
    for c in numeral[::-1]:
        val = literals[c]
        if val < min_char:
            res -= val
        else:
            res += val
            min_char = val
    return res

with open("89.txt", "r") as infile:
   numerals = infile.readlines() 

charcount_initial = sum([len(num.rstrip()) for num in numerals])
print(charcount_initial)

minimal_romans = []
for line in numerals:
    num = line.rstrip()
    x = decode_roman(num)
    minimal_romans.append(apply_subtraction_rule(simple_roman(x)))

charcount_final = sum([len(num) for num in minimal_romans])
print(charcount_final)
print(charcount_initial - charcount_final)
