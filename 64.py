from math import sqrt
from fractions import Fraction

odd_period_count = 0

for x in [i for i in range(10001) if sqrt(i) != int(sqrt(i))]:
    num = []
    den = []
    cs = []
    c = 1
    n = 0
    d = 1
    curr = sqrt(x)
    cycle_complete = False
    cycle_length = 0
    while not cycle_complete:
        n -= int(curr) * d
        c = d
        d = x - n**2
        n = -n
        c, d = Fraction(c, d).numerator, Fraction(c, d).denominator
        curr = c * (sqrt(x) + n) / d
        for i in range(len(num)):
            if num[i] == n and den[i] == d and cs[i] == c:
                cycle_complete = True
                cycle_length = len(num) - i
        den.append(d)
        num.append(n)
        cs.append(c) 

    if cycle_length % 2 == 1:
        odd_period_count += 1

print(odd_period_count)
