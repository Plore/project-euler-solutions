from tools import gcd
from time import sleep

lower_bound = 0
num = 0
for d in range(2, 1000000):
    n = int(lower_bound * d)
    while n / d < 3 / 7:
        if gcd(n, d) == 1:
            lower_bound = n / d
        n += 1
    num = n - 1

print(num)
