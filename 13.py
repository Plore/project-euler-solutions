nums = []
with open("13.txt", "r") as file:
    for num in file.readlines():
        nums.append(int(num.strip()))

print(str(sum(nums))[:10])
