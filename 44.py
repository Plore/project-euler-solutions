def penta(n):
    return n * (3 * n - 1) / 2

def is_penta(m):
    return int((1 + (1 + 24 * m)**0.5) / 6)  == (1 + (1 + 24 * m)**0.5) / 6

n = 2
found = False
while not found:
    p = penta(n)
    m = n - 1   # going backwards from P_n ensures the smallest difference is found first
    q = penta(m)
    while q > 3 * n - 2: # if P_m is smaller than the difference between P_n and P_{n-1}, P_n - P_m cannot be pentagonal 
        q = penta(m)
        if is_penta(p - q) and is_penta(p + q):
            print("{}: {} - {}: {} = {}".format(n, p, m, q, p - q))
            found = True
            break
        m -= 1
    n += 1
