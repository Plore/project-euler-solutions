from tools import eratosthenes, is_prime

# the sequence will be longer than 20 primes, so to stay below the one million total,
# regard only primes smaller than 1000000 / 20
primes = eratosthenes(50000)

maxlen = 0
maxprime = 0
for i in range(len(primes)):
    p_start = primes[i]
    series_total = p_start
    k = 1
    while series_total < 1000000 and i + k + 1 < len(primes):
        if k > maxlen and is_prime(series_total):
            maxlen = k
            maxprime = series_total
        series_total += primes[i + k] + primes[i + k + 1]
        k += 2

print("{}: {} summands".format(maxprime, maxlen))
