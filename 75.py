from math import sqrt
from tools import gcd

# slightly disappointing solution: use the generating formula from wikipedia to generate triplets

limit = 1500000
sols = [0]*limit

for n in range(1, int(limit / 4) + 1):
    m = n + 1
    while m * (m + n) < limit / 2:
        k = 1
        while 2 * k * m * (m + n) < limit:
            a = k * (m**2 + n**2)
            b = k * (2 * m * n)
            c = k * (m**2 - n**2)
            L = a + b + c
            sols[L] += 1
            k += 1
        m += 2
        while not gcd(n, m) == 1:
            m += 2

print(sols.count(1))
