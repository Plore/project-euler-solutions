from math import sqrt
import numpy as np

def fibonacci(n):
    """generator for the n first fibonacci numbers"""
    a = 0
    b = 1
    count = 0
    while count < n:
        yield b
        s = a + b
        a = b
        b = s
        count += 1


# prime stuff

def is_prime(x):
    if x <= 1:
        return False
    if x == 2 or x == 3:
        return True
    if x % 2 == 0 or x % 3 == 0:
        return False
    for m in range(5, int(sqrt(x)) + 1, 6):
        if x % m == 0 or x % (m + 2) == 0:
            return False
    return True

def eratosthenes(n):
    """eratosthenes sieve, calculates primes up to n"""
    nums = np.arange(n + 1)
    for i in range(2, int(sqrt(n)) + 2):
        if nums[i] != 0:
            for j in range(2, int(n / nums[i]) + 1):
                nums[nums[i] * j] = 0
    return list(filter(lambda x: x != 0, nums))[1:]

def prime_factors(x):
    factors = []
    d = 2
    while x >= d**2:
        while x % d == 0:
            factors.append(d)
            x /= d
        d += 1
    if x > 1:
        factors.append(int(x))
    return factors


# other utilities

def is_palindrome(x):
    l = list(str(x))
    l.reverse()
    return int("".join(l)) == x

def gcd(a, b):
    """greatest common divisor using Euclid's algorithm"""
    while b > 0:
        a, b = b, a % b
    return a

def lcm(a, b):
    """least common multiple"""
    return a * b / gcd(a, b)

def lcmm(args):
    if len(args) == 1:
        return args[0]
    else:
        return lcm(args[0], lcmm(args[1:]))

def perm(a, b):
    freqs_a = [0]*10
    freqs_b = [0]*10
    for c in str(a):
        freqs_a[int(c)] += 1
    for c in str(b):
        freqs_b[int(c)] += 1
    for x in range(10):
        if freqs_b[x] != freqs_a[x]:
            return False
    return True

def divisors(num):
    div = [1]
    for d in range(2, int(num**0.5) + 1):
        if num % d == 0:
            div.append(d)
            div.append(num / d)
    if num**0.5 == int(num**0.5):   # avoid including sqrt(num) twice
        div = div[:-1]
    return div
