import numpy as np

with open("82.txt", "r") as infile:
    inp = infile.readlines()

matrix = []
for line in inp:
    matrix.append([int(c) for c in line.rstrip('\n').split(',')])

d = len(matrix[0])
minimal_path_sum = np.zeros(shape=np.shape(matrix))
for y in range(0,d):
    # going up and down on the left only increases the path sum since all items are positive
    minimal_path_sum[y][0] = matrix[y][0]

matrix = np.array(matrix)
for x in range(1, d):
    for y in range(0, d):
        sums = []
        for y_origin in range(0, d):
            sums.append(minimal_path_sum[y_origin][x-1] + sum(matrix[min(y, y_origin):max(y, y_origin)+1, x]))
        minimal_path_sum[y][x] = min(sums)

print(min(minimal_path_sum[:,-1]))
