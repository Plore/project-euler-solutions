from tools import is_prime

for n in [x for x in range(3, 1000000, 2) if not is_prime(x)]:
    goldbach = False
    for m in range(1, int((n / 2)**0.5 + 1)):
        if is_prime(n - 2 * m**2):
            goldbach = True
            break
    if not goldbach:
        print(n)
        break
