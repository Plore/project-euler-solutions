from itertools import permutations
from tools import is_prime
from math import factorial

largest = 0

for n in range(9, 3, -1):
    p = permutations(range(1, n+1))
    k = 0
    while k < factorial(n):
        x = int(''.join(map(str, next(p))))
        if is_prime(x):
            largest = max(x, largest)
        k += 1

print(largest)
