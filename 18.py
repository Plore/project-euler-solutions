# use dynamic programming for this one

nums = []
sums = []
with open("18.txt", "r") as file:
    for line in file.readlines():
        nums.append([int(x) for x in line.strip().split(" ")])
        sums.append([0] * len(nums[-1]))

# boundary conditions
for i in range(len(nums)):
    sums[i][0] = nums[i][0] + sums[i - 1][0]
    sums[i][i] = nums[i][i] + sums[i - 1][i - 1]

for y in range(2, len(nums)):
    for x in range(1, len(nums[y])-1):
        sums[y][x] = max(sums[y - 1][x], sums[y - 1][x - 1]) + nums[y][x]

print(max(sums[len(sums) - 1]))
