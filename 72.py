from tools import eratosthenes

primes = eratosthenes(1000000)
totients = [1] * 1000001

for p in primes:
    totients[p] = p - 1

total = 0

for d in range(2, 1000001):
    if totients[d] > 1:
        total += d - 1
        continue
    x = d
    f = 2
    while x % f != 0:
        f += 1
    totients[d] *= f - 1
    x /= f
    while x % f == 0:
        totients[d] *= f
        x /= f
    totients[d] *= totients[int(x)]
    total += totients[d]

print(total)
