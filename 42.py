def is_triangle(t):
    return (1 + (1 + 8 * t)**0.5) / 2 == int((1 + (1 + 8 * t)**0.5) / 2)

with open("42.txt", "r") as f:
    words = f.read().replace('"', '').split(',')

count = 0
for w in words:
    if is_triangle(sum([ord(x) - 64 for x in w])):
        count += 1

print(count)
