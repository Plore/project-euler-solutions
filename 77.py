import numpy as np
from tools import eratosthenes, is_prime


primes = eratosthenes(100)
print(primes)
ways = np.zeros(1000)

def construct(coll, limit, depth):
    if coll > 100:
        return 
    ways[coll] += 1
    if depth == 0:
        return
    else:
        for i in range(0, limit+1):
            construct(coll + primes[i], i, depth - 1)
        

#for n in range(11):
#    for p in primes:
#        if p > n/2:
#            break
#        ways[n] += ways[n-p]
#        print("{} = {} + {} - > {} ways".format(n, p, n-p, ways[n]))
#    if is_prime(n):
#        ways[n] += 1
#        print("{} = {} - > {} ways".format(n, n, ways[n]))

construct(0, len(primes)-1, 50)
for i in range(len(ways)):
    if ways[i] > 5000:
        print("{} -> {}".format(i, ways[i]))
