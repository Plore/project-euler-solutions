day = 0;
counter = 0;

day = (day + 365) % 7   # 1900 was not a leap year

for year in range(1901, 2001):
    if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
        february = 29
    else:
        february = 28

    month_lengths = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    for m in month_lengths:
        if day == 6:
            counter += 1
        day = (day + m) % 7

print(counter)
