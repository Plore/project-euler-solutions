from itertools import combinations

d1 = list(combinations([0,1,2,3,4,5,6,7,8,9], 6))
d2 = list(combinations([0,1,2,3,4,5,6,7,8,9], 6))
count = 0
squares = [(0,1), (0,4), (0,9), (1,6), (2,5), (3,6), (4,9), (6,4), (8,1)]
for x in d1:
    if 6 in x:
        x += (9,)
    elif 9 in x:
        continue
    for y in d2:
        if 6 in y:
            y += (9,)
        elif 9 in y:
            continue
        possible = True
        for s in squares:
            if not((s[0] in x and s[1] in y) or (s[0] in y and s[1] in x)):
                possible = False
                break
        if possible:
            count += 1
            print(x, y)

print(count)
