from tools import is_prime

def make_product_partitions(num):
    res = [[num]]
    if is_prime(num):
        return res

    for d in range(2, int(num**0.5) + 1):
        if num % d == 0:
            for p in make_product_partitions(num // d):
                p.append(d)
                res.append(p)
    return res

inf = 1000000
minnums = [inf]*12000
n = 1

while max(minnums[2:]) == inf:
    n += 1
    if is_prime(n):
        continue
    product_partitions = make_product_partitions(n)[1:]
    for pp in product_partitions:
        factorsum = sum(pp)
        k = n - factorsum + len(pp)
        if k < 12000 and n < minnums[k]:
            minnums[k] = n


print(sum(set(minnums[2:])))
