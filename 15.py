# this one is simple combinatorics:
# 
# there are fourty edges to traverse, twenty of which must be traversed from left to right and twenty from top to bottom
# the solution then is equal to the number of possibilities for drawing 20 specific chunks out of forty, or 40 over 20

from math import factorial

print(factorial(40) / factorial(20)**2)
