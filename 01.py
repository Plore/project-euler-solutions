import numpy as np

mult_of_three = np.arange(3, 1000, 3)
mult_of_five = np.arange(5, 1000, 5)
mult_of_fifteen = np.arange(15, 1000, 15)

print(np.sum(mult_of_three) + np.sum(mult_of_five) - np.sum(mult_of_fifteen))
