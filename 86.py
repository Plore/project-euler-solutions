from math import sqrt
from tools import gcd
import numpy as np

# if the box dimensions are called a, b and c, then the minimal length is e.g. L**2 = a**2 + (b + c)**2
# thus, generate pythagorean triples L, a, (b + c) with the constraint a >= b >= c until there are over one million solutions a, b, c

def triplets(limit):
    sols = []
    for n in range(1, int(limit / 4) + 1):
        m = n + 1
        while not gcd(n, m) == 1:
            m += 2
        while m * (m + n) < limit / 2:
            k = 1
            while 2 * k * m * (m + n) < limit:
                a = k * (m**2 + n**2)
                b = k * (2 * m * n)
                c = k * (m**2 - n**2)
                sols.append([a, b, c])
                k += 1
            m += 2
            while not gcd(n, m) == 1:
                m += 2
    return sols

target = 1000000
L, x, y = np.array(triplets(target/10)).T
a = np.append(x, y)
bc = np.append(y, x)

M = 0
count = 0

while count < target:
    M += int((target - count) / 2000) + 1
    count = 0
    for i in range(len(a)):
        ai = a[i]
        bci = bc[i]
        if ai <= M and bci <= 2 * ai:
            if bci <= ai:
                count += int(bci / 2)
            else:
                count += int(bci / 2) - (bci - ai) + 1

print("M = {} -> {} integer solutions".format(M, count))
