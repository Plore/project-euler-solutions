from tools import prime_factors

count = 0

for d in range(2, 12001):
    pf = set(prime_factors(d))
    n = int(1 / 3 * d) + 1
    while n / d < 1 / 2:
        coprime = True
        for p in pf:
            if n % p == 0:
                coprime = False
                break
        if coprime:
            count += 1
        n += 1

print(count)
