from math import sqrt
from time import sleep

# for integer area and sidelengths a,a,a+-1 we need 3 / 16 * a^4 -+ a^3 / 8 - a^2 / 16 = m^2
# count by increasing m and checking if integer a exists

counter = 0
m = 1
a = int(sqrt(m) * (16 / 3)**0.25)

while 3 * a + 1 < 1e9:
    print(m)
    x = 3 / 16 * a**4 - a**3 / 8 - a**2 / 16
    while x < m**2 :
        a += 1
        x = 3 / 16 * a**4 - a**3 / 8 - a**2 / 16
    if x == m**2 and 3 * a + 1 < 1e9:
        print(a, x, sqrt(x))
        sleep(0.1)
        counter += 1
    m += 1
    a = int(sqrt(m) * (16 / 3)**0.25)

print(counter)
