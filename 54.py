def score(values, suits):

    values.sort(reverse=True)

    # multiple cards of same value
    freqs = [0] * 13
    for v in values:
        freqs[v] += 1
    for v in values:
        rest = [x for x in values if x != v]
        if freqs[v] == 4:
            return 8, v, rest[0] # four of a kind
        if freqs[v] == 3:
            if max([freqs[r] for r in rest]) == 2:
                return 7, v, rest[0] # full house
            else:
                return 4, v, int(str(rest[0]) + str(rest[1])) # three of a kind
        if freqs[v] == 2:
            for r in rest:
                if freqs[r] == 2:
                    return 3, max(v, r), int(str(min(v, r)) + str(max([i for i in rest if i != r]))) # two pairs
            return 2, v, int(''.join([str(r) for r in rest])) # one pair 

    # consecutive values and/or same suit
    flush = True
    straight = False
    for s in suits[1:]:
        if s != suits[0]:
            flush = False
    if values[0] - values[-1] == 4:
        straight = True

    if straight and flush:
        if values[0] == 12:
            return 10, 0, 0
        else:
            return 9, values[0], 0
    if flush:
        return 6, values[0], 0
    if straight:
        return 5, values[0], 0

    # default
    maxval = max(values)
    values.remove(max(values))
    return 1, maxval, int(''.join([str(x) for x in values])) # high card

def mapping(inval):
    res = inval
    for i in range(len(inval)):
        res = res.replace(inval[i], str(transdict[inval[i]]))
    return int(res)

hands = []
with open("54.txt", "r") as f:
    hands = [line.rstrip() for line in f]

transdict = {'2':0, '3':1, '4':2, '5':3, '6':4, '7':5, '8':6, '9':7, 'T':8, 'J':9, 'Q':10, 'K':11, 'A':12, 'C':0, 'D':1, 'H':2, 'S':3}

total_one = total_two = 0
for hand in hands:
    hand_one = hand[:15].rstrip().split(' ')
    hand_two = hand[15:].split(' ')
    values_one, suits_one = [mapping(card[0]) for card in hand_one], [mapping(card[1]) for card in hand_one]
    values_two, suits_two = [mapping(card[0]) for card in hand_two], [mapping(card[1]) for card in hand_two]
    a1, b1, c1 = score(values_one, suits_one)
    a2, b2, c2 = score(values_two, suits_two)
    if a1 > a2 or (a1 == a2 and b1 > b2) or (a1 == a2 and b1 == b2 and c1 > c2):
        total_one += 1
    else:
        total_two += 1

print(total_one)
