from tools import eratosthenes

primes = eratosthenes(10000)

lim = 50000000
limit_fourth = int(lim**0.25) + 1
limit_third  = int(lim**0.3333) + 1
limit_second = int(lim**0.5)  + 1

foo = [0]*lim

for x in primes:
    if x > limit_second:
        break
    for y in primes:
        if y > limit_third or x**2 + y**3 > lim:
            break
        for z in primes:
            if z > limit_fourth or x**2 + y**3 + z**4 >= lim:
                break
            else:
                foo[x**2 + y**3 + z**4] = 1
            
print(foo.count(1))
