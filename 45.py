def triangle(n):
    return int(n * (n + 1) / 2)

def penta(n):
    return int(n * (3 * n - 1) / 2)

def hexa(n):
    return n * (2 * n - 1)

n = 286
while True:
    # starting points deduced by algebraic manipulation
    m = int(n / 3**0.5)
    h = int(n / 2)

    while penta(m) < triangle(n):
        m += 1
    while hexa(h) < triangle(n):
        h += 1

    if penta(m) == triangle(n) and hexa(h) == triangle(n):
        print("{} {} {} -> {} {} {}".format(n, m, h, triangle(n), penta(m), hexa(h)))
        break

    n += 1
