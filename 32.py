from itertools import permutations

perm = permutations(range(1, 10))
products = []
n = 0

while n < 362880:
    p = next(perm)
    for i in range(1, 3): # first factor may contain one or two digits, second one three or four -> no duplicity
        x = int(''.join(map(str, p[:i])))
        y = int(''.join(map(str, p[i:5])))
        z = int(''.join(map(str, p[5:]))) # products can only be four digits long
        if x * y == z:
           print("{} * {} = {}".format(x, y, z))
           if not z in products:
               products.append(z)
    n += 1

print(sum(products))
