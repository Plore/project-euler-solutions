from tools import divisors, is_prime
from time import sleep

limit = 1000000
aliquot = [0] * (limit + 1)
for div in range(1, int(limit/2)):
    for q in range(2, int(limit/div) + 1):
        aliquot[div * q] += div

lengths = [0] * (limit + 1)
maxlen = 0

print("generated aliquot table")

n = 2
while n < limit:
    n += 1
    if lengths[n] > 0:
        continue
    chain = [n]
    m = aliquot[n]
    cont = True
    while cont:
        if m > limit:
            cont = False
            break
        if m < n:
            for c in chain:
                lengths[c] = lengths[m]
            cont = False
            break
        chainlen = len(chain)
        for i in range(chainlen):
            if m == chain[i]:
                for c in chain:
                    lengths[c] = chainlen - i
                cont = False
                break
        chain.append(m)
        m = aliquot[m]
    if lengths[n] > 10:
        maxlen = lengths[n]
        print(n, lengths[n], chain)
