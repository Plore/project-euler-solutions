with open("22.txt", "r") as f:
    names = f.read().replace('"', '').split(',')

names.sort()

print(sum([(i + 1) * sum([ord(c) - ord('A') + 1 for c in names[i]]) for i in range(len(names))]))
