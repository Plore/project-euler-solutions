import string
from math import sqrt, log10
from itertools import permutations

def is_anagram(a, b):
    if len(a) != len(b):
        return False
    freqs_a = [0]*26
    freqs_b = [0]*26
    for c in a:
        freqs_a[ord(c) - ord('A')] += 1
    for c in b:
        freqs_b[ord(c) - ord('A')] += 1
    for i in range(len(freqs_a)):
        if freqs_a[i] != freqs_b[i]:
            return False
    return True

def is_square(x):
    return sqrt(x) - int(sqrt(x)) == 0

with open("98.txt", "r") as infile:
    words = [word.replace('"', '') for word in infile.readlines()[0].split(',')]

words_a = []
words_b = []
for i in range(len(words)):
    for j in range(i):
        if is_anagram(words[i], words[j]):
            words_a.append(words[i])
            words_b.append(words[j])

maxsquare = 0

for i in range(len(words_a)):
    letters = []
    for c in words_a[i]:
        if not c in letters:
            letters.append(c)
    for digits in list(permutations("0123456789", len(letters))):
        if len(letters) == len(words_a[i]):
            num_a = int(''.join(digits))
            if is_square(num_a):
                num_b = int(words_b[i].translate({ord(x) : y for (x, y) in zip(letters, digits)}))
                if is_square(num_b) and int(log10(num_b)) == int(log10(num_a)):
                    maxsquare = max(maxsquare, num_a, num_b)
        else:
            num_a = int(words_a[i].translate({ord(x): y for (x, y) in zip(letters, digits)}))
            if is_square(num_a):
                num_b = int(words_b[i].translate({ord(x) : y for (x, y) in zip(letters, digits)}))
                if is_square(num_b) and int(log10(num_b)) == int(log10(num_a)):
                    maxsquare = max(maxsquare, num_a, num_b)

print(maxsquare)
