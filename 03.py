from tools import is_prime

number = 600851475143

largest = 0
for i in range(2, int(number**0.5) + 2):
    if is_prime(i) and number % i == 0:
        largest = i
        
print(largest)
