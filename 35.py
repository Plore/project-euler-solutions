from time import sleep
from tools import is_prime, eratosthenes

def rot_one(num):
    n = len(str(num))
    return (num % 10**(n - 1)) * 10 + num // 10**(n - 1)

def circular_prime(num):
    s = str(num)
    if '0' in s or '4' in s or '6' in s or '8' in s:
        return False
    if len(s) > 1:
        if '2' in s or '5' in s:
            return False
    for r in range(len(s) - 1):
        num = rot_one(num)
        if not is_prime(num):
            return False
    return True

primes = eratosthenes(1000000)
count = 0

for p in primes:
    if circular_prime(p):
        count += 1

print(count)
