from itertools import permutations
def triangle(n):
    return int(n * (n + 1) / 2)

def square(n):
    return n**2

def penta(n):
    return int(n * (3 * n - 1) / 2)

def hexa(n):
    return n * (2 * n - 1)

def hepta(n):
    return int(n * (5 * n - 3) / 2)

def octa(n):
    return n * (3 * n - 2)

nums = [[func(x) for x in range(200) if len(str(func(x))) == 4] for func in [triangle, square, penta, hexa, hepta, octa]]

perms = list(permutations([0,1,2,3,4,5]))[:120]

for p in perms:
    for a in nums[p[0]]:
        for b in [x for x in nums[p[1]] if int(str(x)[:2]) == int(str(a)[2:])]:
            for c in [x for x in nums[p[2]] if int(str(x)[:2]) == int(str(b)[2:])]:
                for d in [x for x in nums[p[3]] if int(str(x)[:2]) == int(str(c)[2:])]:
                    for e in [x for x in nums[p[4]] if int(str(x)[:2]) == int(str(d)[2:])]:
                        for f in [x for x in nums[p[5]] if int(str(x)[:2]) == int(str(e)[2:])]:
                            if int(str(f)[2:]) == int(str(a)[:2]):
                                print("{} {} {} {} {} {} -> {}".format(a, b, c, d, e, f, a + b +c + d + e + f))
