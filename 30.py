# numbers higher than 299999 can only yield ratios of number to sum-of-digits-to-the-fifth of less than one

total = 0
for n in range(2, 300000):
    if sum([int(c)**5 for c in str(n)]) == n:
        total += n
        print(n)

print("Sum: {}".format(total))
