from tools import divisors

total = 0
for n in range(10000):
    d = int(sum(divisors(n)))
    if d > n and d < 10000 and sum(divisors(d)) == n:
        total += n + d

print(total)
