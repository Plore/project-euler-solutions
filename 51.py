from tools import eratosthenes, is_prime

# to yield eight primes, only a multiple of three digits may be replaced, as otherwise 
# three out of ten numbers generated will be divisible by three

primes = eratosthenes(1000000) # bold claim that the primes will contain at most six digits

sol = []
for p in primes:
    for d in range(10):
        # at six digits max, one member of the family will contain identical digits only at the replacement positions
        if str(p).count(str(d)) == 3:             
            digit_places = [i for i, c in enumerate(str(p)) if c == str(d)]
            if 6 == digit_places[-1]:
                break
            if 0 == digit_places[0]:
                reps = range(1, 10)
            else:
                resp = range(10)
            n = 0
            for j in reps:
                p_replace = str(p).replace(str(d), str(j))
                if is_prime(int(p_replace)):
                    n += 1
            if n > 7:
                sol.append(p)

print(min(sol))
