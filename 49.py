import numpy as np
from tools import eratosthenes, is_prime, perm

primes = [x for x in eratosthenes(10000) if x > 1000]

for p in primes:
    for q in [y for y in primes if y > p]:
        if perm(p, q):
            r = q + (q - p)
            if is_prime(r) and perm(r, q):
                print(p, q, r)
