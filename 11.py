from operator import mul
from functools import reduce
import numpy as np

grid = []
with open("11.txt", "r") as file:
    for line in file.readlines():
        grid.append([int(x) for x in line.strip().split(" ")])

grid = np.array(grid)
largest = 0

# left / right / up / down
for x in range(20):
    for y in range(17):
        largest = max(largest, reduce(mul, grid[x][y:y + 4]))
        largest = max(largest, reduce(mul, grid.T[x][y:y + 4]))

# diagonally
for x in range(17):
    for y in range(17):
        largest = max(largest, reduce(mul, [grid[x + k][y + k] for k in range(4)]))
        largest = max(largest, reduce(mul, [grid[x + k][y + 3 - k] for k in range(4)]))

print(largest)
