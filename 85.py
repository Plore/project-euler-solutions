def num_rectangles(dim_x, dim_y):
    res = 0
    for y in range(1, dim_y+1):
        for x in range(1, dim_x+1):
            res += (dim_y - y + 1) * (dim_x - x + 1)
    return res

# a grid of dimensions 1x2000 has 2001000 rectangles
# this yields 2000 as the maximal extent as well as a starting eps
eps = 1000

for dimx in range(1, 2000):
    for dimy in range(1, dimx+1):
        nr = num_rectangles(dimx, dimy)
        if nr > 2000000 + eps:
            break
        if abs(num_rectangles(dimx, dimy) - 2000000) < eps:
            eps = abs(num_rectangles(dimx, dimy) - 2000000)
            print("{} x {} = {} -> {} rectangles".format(dimx, dimy, dimx * dimy, nr))
