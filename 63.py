from math import ceil, floor

count = 0

for n in range(1, 100):
    for x in range(ceil(10**((n - 1) / n)), 10):
        if len(str(x**n)) == n:
            count += 1

print(count)
