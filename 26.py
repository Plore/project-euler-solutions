def cycle_length(d):
    nums = [1]
    num = 1
    while True:
        if num == 0:
            return 1
        while num < d:
            num *= 10
        num %= d
        nums.append(num)
        for i in range(len(nums) - 1):
            if nums[i] == num:
                return len(nums) - 1 - i 

maxd = 0
maxlen = 0
for d in range(5, 1000):
    if cycle_length(d) > maxlen:
        maxlen = cycle_length(d)
        maxd = d

print(maxd)
