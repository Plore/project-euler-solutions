from tools import eratosthenes, perm

# To minimise n / phi(n), minimise the number of distinct prime factors of n. 
# Since p and p - 1 can never be permutations of each other, however, at least two prime factors p1 and p2 have to be used. 

# bold claim of the minimal ratio being smaller than 1.001 - if prime factor p1 exceeds 10000, p2 has to stay below 1000, yielding p2 / (p2 - 1) > 1.001
primes = eratosthenes(10000)

minratio = 2
for i in range(len(primes)-1, 0, -1):
    for j in range(i, 0, -1):
        test = primes[i] * primes[j]
        if test > 10000000:
            continue
        if i == j:
            totient = primes[i] * (primes[i] - 1)
        elif j < i:
            totient = (primes[i] - 1) * (primes[j] - 1)
        if test / totient < minratio and perm(test, totient):
            minratio = test / totient
            minnum = test
            #print("{} / {} = {}  {}".format(test, totient, test / totient, [primes[i], primes[j]]))
            
print(minnum)
