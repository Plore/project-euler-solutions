# If n is composed of the distinct prime factors p1 ... pk with occurences m1 ... mk, the totient can be computed as phi(n) = p1 ^ (m1 - 1) * (p1 - 1) * ... * pk ^ (mk - 1) * (pk - 1). 
# Hence, if the ratio n / phi(n) is to be maximised, don't use any prime factor more than once. 
# Moreover, small primes p maximise the ratio p / (p - 1), so n / phi(n) is maximised by numbers of the form (2 * 3 * 5 * ...) / (1 * 2 * 4 * ...).

print(2 * 3 * 5 * 7 * 11 * 13 * 17)
