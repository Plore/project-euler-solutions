nums = []
dens = []
numshort = []
denshort = []
for n in range(11, 100):
    for d in range(n + 1, 100):
        n0 = int(str(n)[0])
        n1 = int(str(n)[1])
        d0 = int(str(d)[0])
        d1 = int(str(d)[1])
        # need to check only for crosswise cancelling
        if (n0 == d1 and d0 != 0 and n1 / d0 == n / d):
            nums.append(n)
            dens.append(d)
            numshort.append(n1)
            denshort.append(d0)
        if (n1 == d0 and d1 != 0 and n0 / d1 == n / d):
            nums.append(n)
            dens.append(d)
            numshort.append(n0)
            denshort.append(d1)

dprod = nprod = 1
for n, d, ns, ds in zip(nums, dens, numshort, denshort):
    print("{} / {} -> {} / {}".format(n, d, ns, ds))
    nprod *= n
    dprod *= d

print(dprod / nprod)
