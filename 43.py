from itertools import permutations

def divisible(tup, num):
    return int(''.join(map(str, tup))) % num == 0

p = permutations(range(10))
n = 0
total = 0
j
while n < 3628800:
    x = next(p)
    if x[3] % 2 == 0 and (x[5] == 5 or x[5] == 0) and sum(x[2:5]) % 3 == 0 and divisible(x[4:7], 7) and divisible(x[5:8], 11) and divisible(x[6:9], 13) and divisible(x[7:], 17):
        print(x)
        total += int(''.join(map(str, x)))
    n += 1

print(total)
