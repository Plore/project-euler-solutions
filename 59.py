with open("59.txt", "r") as f:
    letters = [int(c) for c in f.read().split(',')]

def decrypt(text, key):
    n = len(key)
    res = [0]*len(text)
    for i in range(len(text)):
        res[i] = text[i] ^ key[i % n]
    return res

for a in range(97, 123):
    for b in range(97, 123):
        for c in range(97, 123):
            text = ''.join([chr(c) for c in decrypt(letters, [a, b, c])])
            if " the " in text:
                print(text)
                print(sum(decrypt(letters, [a, b, c])))
