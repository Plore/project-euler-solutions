from tools import prime_factors

x = 2

while True:
    if len(set(prime_factors(x))) > 3 and len(set(prime_factors(x + 1))) > 3 and len(set(prime_factors(x + 2))) > 3 and len(set(prime_factors(x + 3))) > 3:
        print("{} -> {}".format(x, prime_factors(x)))
        print("{} -> {}".format(x + 1, prime_factors(x + 1)))
        print("{} -> {}".format(x + 2, prime_factors(x + 2)))
        print("{} -> {}".format(x + 3, prime_factors(x + 3)))
        break
    x += 1
