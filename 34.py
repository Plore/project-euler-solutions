from math import factorial

total = 0
# since 7 * 9! / 9999999 < 1, higher numbers don't have to be checked as they would produce an even smaller ratio
for n in range(3, 7 * factorial(9)):
    facsum = sum([factorial(int(c)) for c in str(n)])
    if facsum == n:
        total += n

print(total)
