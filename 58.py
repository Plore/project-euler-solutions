from tools import is_prime

n_prime = 0
x = l = 1
while True:
    for k in range(3):
        x += 2 * l
        if is_prime(x):
            n_prime += 1
    x += 2 * l
    if n_prime / (4 * l + 1) < 0.1:
        break
    l += 1

print(2 * l + 1)
