max_n = 0
max_count = 0
for n in range(3, 1001):
    count = 0
    for a in range(1, int(n / 2) + 1):
        b = (n**2 - 2 * n * a) / (2 * n - 2 * a)
        c = n - a - b
        if b == int(b):
            count += 1
    if count > max_count:
        max_count = count
        max_n = n

print("{}: {} possibilities".format(max_n, max_count))
