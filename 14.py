import numpy as np

maxlength = 1
lengths = np.ones(1000000)
for j in range(1, 1000000):
    length = 0
    i = j
    while i >= j and i != 1:  
        length += 1
        if i % 2 == 0:
            i /= 2
        else:
            i = 3 * i +1
    length += lengths[i]    # recycle knowledge gained from previous chain
    lengths[j] = length
    maxlength = max(length, maxlength)
    if maxlength == length:
        print("Number "+str(j)+" has produced a chain of length "+str(length))

