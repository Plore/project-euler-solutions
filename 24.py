from itertools import permutations
from math import factorial

perms = permutations(range(10))
n = 0
while n < 1000000:
    x = next(perms)
    n += 1

print(x)

# alternative solution without cheating

digits = list(range(10))
solution = []
rest = 1000000
while len(digits) > 0:
    i = 0
    while rest > factorial(len(digits) - 1):
        rest -= factorial(len(digits) - 1)
        i += 1
    solution.append(digits[i])
    digits.remove(digits[i])

print(solution)
