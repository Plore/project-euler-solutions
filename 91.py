import numpy as np

# geometrical insights: Call the points O, P = (xP, yP) and Q = (xQ, yQ). Then
# 1) To prevent double counting, yP / xP > yQ / xQ
# 2) If xP > xQ, the line from Q to P has positive slope and no right angles are possible
# 3) Therefore, xQ = 0 forces the triangle onto the line x = 0

count = 0
lim = 51
for xQ in range(1, lim):
    print(xQ)
    for yQ in range(0, lim):
        for xP in range(0, xQ + 1):
            for yP in range(int(yQ / xQ * xP + 1e-5) + 1, lim):
                a = (yP-yQ)**2 + (xP-xQ)**2
                b = yP**2 + xP**2
                c = yQ**2 + xQ**2
                if a + b == c or a + c == b or b + c == a:
                    count +=1
       #             print(xP, yP, xQ, yQ)
print(count)
