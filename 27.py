from tools import is_prime

maxn = maxa = maxb = 0
for a in range(-1000, 1001):
    for b in range(-1000, 1001):
        n = 0
        while is_prime(n**2 + a * n + b):
            n += 1
        if n > maxn:
            print("{} {} -> {}".format(a, b, n))
            maxa = a
            maxb = b
            maxn = n

print(maxa * maxb)
