from time import sleep, time
from tools import eratosthenes, is_prime

primes = eratosthenes(10000)

def compatible(a, b):
    return is_prime(int(str(a) + str(b))) and is_prime(int(str(b) + str(a)))

comps = []

for i in range(len(primes)):
    print(primes[i])
    comps.append([])
    for j in range(i):
        if compatible(primes[i], primes[j]):
            comps[i].append(primes[j])
    c = comps[i]
    if len(c) < 4:
        continue
    for j in range(len(c)):
        for k in range(j):
            if compatible(c[j], c[k]):
                for l in range(k):
                    if compatible(c[j], c[l]) and compatible(c[k], c[l]):
                        for m in range(l):
                            if compatible(c[j], c[m]) and compatible(c[k], c[m]) and compatible(c[l], c[m]):
                                print("{} {} {} {} {} -> {}".format(primes[i], c[j], c[k], c[l], c[m], primes[i] + c[j] + c[k] + c[l] + c[m]))
