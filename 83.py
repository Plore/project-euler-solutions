import numpy as np

with open("83.txt", "r") as infile:
    inp = infile.readlines()

matrix = []
for line in inp:
    matrix.append([int(c) for c in line.rstrip('\n').split(',')])

matrix = np.array(matrix)
d = len(matrix[0])
nope = 10000 * d**2
min_path_sum = np.zeros(shape=np.shape(matrix))
min_path_sum.fill(nope)
min_path_sum[0][0] = matrix[0][0]

current_min = min_path_sum[-1][-1]
finished = False

# standard dynamic programming strategy, but iterate several times to allow for back-winding paths
while not finished:
    for y in range(0,d):
        for x in range(0,d):
            if x == 0 and y == 0:
                continue

            neighbors = [(x-1,y), (x+1,y), (x,y-1), (x,y+1)]
            min_path_sum[y][x] = matrix[y][x] + min([min_path_sum[n[1]][n[0]] for n in neighbors if 0 <= n[0] < d and 0 <= n[1] < d]) 

    if min_path_sum[-1][-1] == current_min:
        finished = True
    else:
        current_min = min_path_sum[-1][-1]

print(current_min)
