# this one can be solved by breaking down letter counts to repeating patterns and adding them manually

count_to_nine = 3 + 3 + 5 + 4 + 4 + 3 + 5 + 5 + 4               # recurring a lot
count_ten_to_nineteen = 3 + 6 + 6 + 8 + 8 + 7 + 7 + 9 + 8 + 8   # also recurring several times
count_tens = 6 + 6 + 5 + 5 + 5 + 7 + 6 + 6                      # "twenty", "thirty" and so on

count_to_ninety_nine = 9 * count_to_nine + 10 * count_tens + count_ten_to_nineteen

# now use 10 times the above count + 100 times the count_to_nine for the hundreds prefixes + 900 times "hundred" + 891 times "and" + "one thousand"
count_total = 10 * count_to_ninety_nine + 100 * count_to_nine + 900 * 7 + 891 * 3 + 11

print(count_total)
