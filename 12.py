from math import sqrt

triangle = 0
for i in range(10000000):
    triangle += i
    factors = []
    for j in range(1, int(sqrt(triangle))+1):
        if triangle % j == 0:
            factors.append(j)
            factors.append(triangle / j)
    if len(factors) > 500:
        print(triangle)
        break

