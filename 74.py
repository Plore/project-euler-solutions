from math import factorial

facs = [factorial(x) for x in range(10)]

lengths = [0] * 1000000
lengths[169] = lengths[363601] = lengths[1454] = 3
lengths[871] = lengths[45361] = 2
lengths[872] = lengths[45362] = 2

count = 0

for n in range(1, 1000000):
    x = n
    y = 0
    length = 0
    while True:
        if x == y:
            break
        if x == 169 or x == 1454 or x == 363601 or x == 871 or x == 872 or x == 45361 or x == 45632:
            length += lengths[x]
            break
        if x < n:
            length += lengths[x]
            break
        y = x
        x = sum([facs[int(c)] for c in str(x)])
        length += 1
    lengths[n] = length
    if length == 60:
        count += 1

print(count)
