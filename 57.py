count = 0
for j in range(1000):
    n = 1
    d = 2
    for k in range(j):
        n += 2 * d
        n, d = d, n
    n += d
    if len(str(n)) > len(str(d)):
        count += 1

print(count)
