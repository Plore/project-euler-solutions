from operator import mul
from functools import reduce

numlist = []
with open("08.txt", "r") as file:
    for line in file.readlines():
        numlist.append(line.rstrip())
nums = [int(x) for x in list(''.join(numlist))]    # separate digits
largest = 0
for start in range(len(nums)-13):
    largest = max(largest, reduce(mul, nums[start:start+13], 1))

print(largest)
